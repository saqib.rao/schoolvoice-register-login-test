describe("First test", () => {
    it("should visit homepage", () => {
        cy.visit(Cypress.config('baseUrl'));
    });

    it("Register New User", () => {
        cy.get('nav > .green-btn').click();
        cy.get('[value="school"]').click();
        cy.get('#roleDirectBtn').click();
        cy.get('#signUpForm > :nth-child(1) > input').type('School Username');
        cy.get('#signUpForm > :nth-child(2) > input').type("School Name");
        cy.get('#phone').type("0569038034");
        cy.get('.email_show').type("owner@schoolvoice.com");
        cy.get(':nth-child(6) > input').type(Cypress.config('password'));

        cy.get('.email_validation').should('be.empty');
        cy.get('.password_validation').should('contain', "Good password");


        cy.get('#signUpForm > .submit-btn').click();
        cy.wait(2000);
        cy.get('#alert').should('be.empty');
        cy.get('#submitFeedback > h1').should('contain', "Registration successful!");
    });

    it("Login User", () => {
        cy.login();
    });
});
